const button = document.getElementById("findButton");
button.onclick = function () {
    let typedText = document.getElementById("input").value;
    let resultado = getAnagramsOf(typedText);
    let variavel = document.createElement('p')
    variavel.textContent = resultado;
    document.body.appendChild(variavel);
}

function alphabetize(a) {
    return a.toLowerCase().split("").sort().join("").trim();
}

function getAnagramsOf(input) {

    let typedTextOrder = alphabetize(input);


    // for (let count = 0; count < allText.length; count++) {
    //     let final;
    //     let position;
    //     if (palavras.includes(allText) === true) {
    //         position = palavras.indexOf(allText);
    //         final = palavras[position];
    //         return final;
    //     }

    //     // seu código vai aqui
    // }
    let tudo = [];
    for (let count = 0; count < palavras.length; count++) {
        let stringDoArray = alphabetize(palavras[count])
        if (stringDoArray === typedTextOrder) {
            tudo.push(palavras[count] + " ");
        }
    }
    return tudo;


    // console.log(tudo);
}
